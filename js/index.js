//声明全局对象的构造方法
function structure(){
	this.obj = [
		{"pages":["pages/index/index","pages/logs/logs"],
		"window":{},
		"tabBar":{"list":[{
      		"pagePath": "pages/index/index",
      		"text": "首页"
    		},{
      		"pagePath": "pages/logs/logs",
      		"text": "日志"
    		}
    	]},
    	"networkTimeout":{}
    }];
	this.addpages = addpages;
	this.addwindow = addwindow;
	this.addtabBar = addtabBar;
	this.addnetworkTimeout = addnetworkTimeout;
	this.configdebug = configdebug;
	this.removepages = removepages;
	this.removetabBar = removetabBar;
	this.show = show;
}
//添加页面路径
function addpages(route){
	this.obj[0].pages.push(route);
	this.show();
}
//添加窗口配置属性
function addwindow(dataname,datastruc){
	if (datastruc) {
		if (datastruc === "true" && dataname === "enablePullDownRefresh") {
			this.obj[0].window[dataname] = true;
		}else if(datastruc === "false" && dataname === "enablePullDownRefresh"){
			this.obj[0].window[dataname] = false;
		}else{
			this.obj[0].window[dataname] = datastruc;
		}
		this.show();
	}
}
//添加tab配置属性
function addtabBar(dataname,datastruc){
	if (datastruc) {
		if (datastruc instanceof Object) {
			this.obj[0].tabBar.list.push(datastruc);
		}else{
			this.obj[0].tabBar[dataname] = datastruc;
		}
		this.show();
	}
}
//添加网络超时的配置属性
function addnetworkTimeout(dataname,datastruc){
	if (datastruc) {
		var datanum = parseInt(datastruc);
		this.obj[0].networkTimeout[dataname] = datanum;
		this.show();
	}
}
//设置debug开启关闭
function configdebug(yn){
	this.obj[0]["debug"] = yn;
	this.show();
}
//删除页面配置
function removepages(numof){
	this.obj[0].pages.splice(numof,1);
	this.show();
}
//删除底部按钮
function removetabBar(index,e){
	this.obj[0].tabBar.list.splice(index,1);
	e.parentNode.parentNode.remove();
	this.show();
}
//输出JSON配置文件预览
function show(){
	var datajson = JSON.stringify(this.obj[0], undefined, 4);
	output(syntaxHighlight(datajson));
}
//将格式化后的JSON数据输出到code视图
function output(inp) {
	document.querySelector(".code-body").innerHTML = "";
    document.querySelector(".code-body").appendChild(document.createElement('pre')).innerHTML = inp;
}
//将obj数组格式化为JSON数据并加标签壳和样式，等待输出
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}
//实例化对象
var struc = new structure();
struc.show();

//页面路径添加操作函数
function addroute(route){
	var routes = "pages/"+route;
	if (route) {
		if (struc.obj[0].pages.indexOf(routes) === -1) {
			struc.addpages(routes);
			document.querySelector("#route").value = "";
			document.querySelector(".runroute").appendChild(document.createElement("li")).innerHTML='<span>'+route+'</span><button onclick="removeroute(this)" type="button" name="removeroute" style="padding:3px 5px;">删除</button>';
		}else{
			document.querySelector("#route").value = "";
			alert("该路径已经添加过了,不可重复添加~");
		}
	}
}	
//页面路径删除操作函数
function removeroute(e) {
	var route = e.previousElementSibling.innerHTML;
	var routes = "pages/"+route;
	var numof = struc.obj[0].pages.indexOf(routes);
	if (numof != -1) {
		struc.removepages(numof);
		e.parentNode.remove();
	}else{
		alert("未搜索到该路径~");
	}
}
//tabBerlist添加按钮函数
function addtabBarlist(){
	var text = document.querySelector("#Text").value;
	var pagePath = document.querySelector("#pagePath").value;
	var iconPath = document.querySelector("#iconPath").value;
	var selectedIconPath = document.querySelector("#selectedIconPath").value;
	var datastruc = {};
	if (pagePath && text) {
		datastruc.text = text;
		datastruc.pagePath = "pages/"+pagePath;
		(iconPath)?datastruc.iconPath = iconPath:iconPath = "";
		(selectedIconPath)?datastruc.selectedIconPath = selectedIconPath:selectedIconPath = "";
		struc.addtabBar("",datastruc);
		document.querySelector("#Text").value = "";
		document.querySelector("#pagePath").value = "";
		document.querySelector("#iconPath").value = "";
		document.querySelector("#selectedIconPath").value = "";
	}else{
		alert("底部按钮的文字和路径是必须添加的~");
	}
}
console.log("该工具是本人国庆假期无聊,在看完微信小程序文档后撸出来的！目前还不觉得这个工具够完善，欢迎提出建议！")
console.log("本来觉得应该可以做一个拖拽式的小程序开发工具,无奈项目太大,我时间有限，独自一人无法驾驭！")
console.log("如果你也是一个JSer,并且在项目架构上有一定的基础,欢迎加我QQ：2450346849 我们一起去“搞事情！”")